This repo contains the code for a Java ContextParticipant.

## Build it

Downloads dependencies, generates gRPC stubs and compiles it all.

    > ./gradlew build

### Generate a jar

    > ./gradlew jar

## Run it

    > java -jar build/libs/JavaContextParticipant.jar --help

Will display helpful information and further info on how to run the participant.

## Generate documentation

Requires [docco](https://github.com/jashkenas/docco). 

    > docco -o docs src/main/java/JavaContextParticipant/*.java

Or run the `docs.sh` shell-script (on Linux/BSD/OSX).


## Examples

### Get list of e.g. ContextManagers

`java -jar build/libs/JavaContextParticipant.jar --port 21111 --host "172.16.239.149" OfType`

### Join Context and record transactions

`java -jar build/libs/JavaContextParticipant.jar --port 21111 --host "172.16.239.149" JoinCommonContext --componentId 90e0358e-a61d-4920-a2b2-58473433b4bd --applicationName bgrm138p43kg008eem3g`


### Change state

`java -jar build/libs/JavaContextParticipant.jar --port 21111 --host "172.16.239.149" WriteState --state "search=bar" --applicationName bjlu8dtqsr7000ae4efg --componentId 90e0358e-a61d-4920-a2b2-58473433b4bd`
