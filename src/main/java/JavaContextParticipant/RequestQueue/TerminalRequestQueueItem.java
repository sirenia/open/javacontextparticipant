package JavaContextParticipant.RequestQueue;

public class TerminalRequestQueueItem implements RequestQueueItem {
    public Throwable error;

    public TerminalRequestQueueItem(Throwable err) {
        error = err;
    }
}
