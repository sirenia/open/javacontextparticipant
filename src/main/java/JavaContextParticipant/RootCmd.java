// # ContextParticipant
// This file contains a method for each example/use-case case.
//
// * For general information about gRPC, see https://grpc.io/docs/guides/](https://grpc.io/docs/guides/
// * For information about the Java implementation of gRPC, see https://grpc.io/docs/tutorials/basic/java.html
// * For API docs on the HTTP mapping, see http://api.seacow.sirenia.eu
// * For the protocol description file, see https://gitlab.com/sirenia/open/javaseacowclient/-/blob/master/src/main/proto/seacow.proto


package JavaContextParticipant;

import JavaContextParticipant.RequestQueue.ActualRequestQueueItem;
import JavaContextParticipant.RequestQueue.RequestQueueItem;
import JavaContextParticipant.RequestQueue.TerminalRequestQueueItem;
import com.google.common.base.Stopwatch;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.grpc.stub.StreamObserver;
import picocli.CommandLine;
import seacow.*;

import javax.net.ssl.SSLException;
import java.io.Console;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.logging.Logger;

// The `RootCmd` delegates to its list of sub-commands implemented as `@CommandLine.Command` annotated methods.
@CommandLine.Command(mixinStandardHelpOptions = true)
public class RootCmd implements Runnable {

  private static final Logger logger = Logger.getLogger(RootCmd.class.getName());

  // The host of the server we'll connect to.
  @CommandLine.Option(names = {"--host"}, description = "The host to connect to", defaultValue = "localhost")
  private String host;

  // The port property determines which port we're connecting to.
  @CommandLine.Option(names = {"-p", "--port"}, description = "The port to connect to", defaultValue = "9000")
  private int port;

  // The port property determines which port we're connecting to.
  @CommandLine.Option(names = {"--cert"}, description = "Which cert to use for the connection", defaultValue = "")
  private String cert;

  // ## Registry examples

  // ### List actions
  // This service is invoked to list the actions available on the service instance which you've connected to.
  @CommandLine.Command(mixinStandardHelpOptions = true)
  public void ListActions() throws SSLException {

    // First, we'll need to obtain a client for the registry service.
    RegistryGrpc.RegistryBlockingStub registry = new SeacowConnection(host, port, cert).getRegistry();

    // Next, we'll invoke the `LocalActions` method with a very sparse request.
    Seacow.LocalActionsResponse lar = registry.localActions(Seacow.LocalActionsRequest.newBuilder().build());

    // Go through the received actions
    for (Seacow.LocalAction la : lar.getActionsList()) {
      // Log out each action
      logger.info(la.toString());
    }
  }

  // ### List items of a specific type
  // This can be used to figure out which ContextManager to connect to. Normally a Manatee will only run one instance
  // but it will spawn more if multiple applications of the same type are discovered.
  @CommandLine.Command(mixinStandardHelpOptions = true)
  public void OfType(@CommandLine.Option(names = {"--typename"}, defaultValue = "ContextManager") String typename) throws SSLException {
    RegistryGrpc.RegistryBlockingStub registry = new SeacowConnection(host, port, cert).getRegistry();

    // We invoke the `OfType` method and supply the `typename`. Currently only "ContextManager" is allowed for Manatee requests.
    Seacow.OfTypeResponse otr = registry.ofType(Seacow.OfTypeRequest.newBuilder().setTypeName(typename).build());

    // The response is a collection of identifiers (strings)
    for (String id : otr.getIdentifiersList()) {
      // For each identifier we fetch a bit more information and log out the result.
      Seacow.ComponentByIDResponse cbir = registry.componentByID(Seacow.ComponentByIDRequest.newBuilder().setIdentifier(id).build());
      logger.info(cbir.toString());
    }
  }
  // ## ContextManager examples

  // ### Join a context session
  // As a context participant you can join a session to
  //
  // - participate in context synchronization,
  // - act as a context agent to perform actions,
  // - or request another agent to perform a given action.
  //
  // When joining a session you need to first decide how to requests coming from the context manager.
  // This involves choosing the protocol on which your participant is reachable. For this example we'll use a gRPC
  // stream to allow the context manager to contact us.
  //
  // Other options include:
  //
  // - running a HTTP server and implementing the context participant,
  // - connecting via a WebSocket and responding to the JSON-RPC invocations channeled here.
  @CommandLine.Command(mixinStandardHelpOptions = true)
  public void JoinCommonContext(
    @CommandLine.Option(
      names = {"--componentId"},
      defaultValue = "",
      description = "The id of the context manager to join"
    ) String componentId,
    @CommandLine.Option(
      names = {"--applicationName"},
      required = true,
      description = "The name identifying the application. This should be an identifier from the CMR."
    ) String applicationName,
    @CommandLine.Option(names = {"--survey"}, defaultValue = "true")
      boolean shouldSurvey,
    @CommandLine.Option(names = {"--wait"}, defaultValue = "true")
      boolean shouldWait
  ) throws InterruptedException, SSLException {

    // Get access to the async context manager stub
    SeacowConnection s = new SeacowConnection(host, port, cert);
    ContextManagerGrpc.ContextManagerStub asyncStub = s.getContextManagerAsync();
    ContextDataGrpc.ContextDataBlockingStub cd = s.getContextData();
    componentId = getComponentIdIfNotSpecified(componentId, s);

    // Initialize a  queue to handle streaming responses
    final LinkedTransferQueue<RequestQueueItem> requestQueue = new LinkedTransferQueue<>();

    // Start observing the stream. Normally we'd care about the return value, but not in this example.
    String finalComponentId = componentId;
    final StreamObserver<Seacow.ContextParticipantStreamRequest> stream = asyncStub.contextParticipantStream(new StreamObserver<Seacow.ContextParticipantStreamResponse>() {
      @Override
      public void onNext(Seacow.ContextParticipantStreamResponse value) {
        // The server will send us a variety of messages, setup a switch to handle each type of message
        switch (value.getContentCase()) {
          // In case we get a reply to our join request.
          case JOIN_COMMON_CONTEXT:
            // We simply log out the details.
            // For a proper implementation we'd need to keep the `ParticipantCoupon` for further interactions with the context manager.
            Seacow.JoinCommonContextResponse join = value.getJoinCommonContext();
            logger.info("Joined: " + join.toString());

            // On joining we need to get the latest state to update the internal state of the application.
            // This example has no internal state to update, but we do it still.
            Seacow.GetItemNamesResponse namesResponse = cd.getItemNames(Seacow.GetItemNamesRequest
              .newBuilder()
              .setComponentIdentifier(finalComponentId)
              // You can use -1 as context coupon to get the latest values or use the cm to get the `MostRecentContextCoupon`.
              .setContextCoupon(-1)
              .build());

            // Write out the changes that happened in the tx.
            Seacow.GetItemValuesResponse values = cd.getItemValues(Seacow.GetItemValuesRequest
              .newBuilder()
              .setComponentIdentifier(finalComponentId)
              // -1 == MostRecentContextCoupon
              .setContextCoupon(-1)
              // We need to say which values we're interested in. We'll use all available names here but
              // we could also just specify the ones we're interested in directly.
              .addAllItemNames(namesResponse.getNamesList())
              .build());

            // Write out the initial state
            logger.info("Initial state immediately after joining = "+values.toString());

            break;

          // *Note* that the context manager expects a timely reply on some of these methods.
          // If a reply is not received by the context manager it may choose to disconnect the stream.

          // A reply for `CONTEXT_CHANGES_PENDING` is expected and we simple always provide an "accept".
          case CONTEXT_CHANGES_PENDING:
            // Log the incoming `CONTEXT_CHANGES_PENDING` message.
            Seacow.ContextParticipantStreamResponse.ContextChangesPendingResponse contextChangesPending = value.getContextChangesPending();
            logger.info("Received CONTEXT_CHANGES_PENDING: " + contextChangesPending.toString());

            // We need the ContextCoupon to figure out which changes happened in this tx.
            long currentTxCoupon = contextChangesPending.getContextCoupon();

            // First we'll figure out which subjects/names are available
            // Lets see which subjects are in the shared context.
            Seacow.GetItemNamesResponse onJoinNames = cd.getItemNames(Seacow.GetItemNamesRequest
              .newBuilder()
              .setComponentIdentifier(finalComponentId)
              // Use the ContextCoupon from the request.
              .setContextCoupon(currentTxCoupon)
              .build());

            // Write out the changes that happened in the tx.
            Seacow.GetItemValuesResponse onJoinValues = cd.getItemValues(Seacow.GetItemValuesRequest
              .newBuilder()
              .setComponentIdentifier(finalComponentId)
              // Use the ContextCoupon from the request.
              .setContextCoupon(currentTxCoupon)
              // We only want the values that were changed in the tx
              .setOnlyChanges(true)
              // We need to say which values we're interested in. We'll use all available names here but
              // we could also just specify the ones we're interested in directly.
              .addAllItemNames(onJoinNames.getNamesList())
              .build());

            // Log the changes.
            // Note that the CCOW specifies that the values returned are structured s.t.
            // the value at index `N` is the *subject* and the value is then stored at `N+1`. E.g. if the
            // context contains `{ Foo: 'bar', Fii: 'bur' }` the values returned will be:
            // `['Foo', 'bar', 'Fii', 'bur']`.
            logger.info("The following changes occurred in tx: "  +currentTxCoupon + ", changes: "+onJoinValues.toString());

            // Build an "accept" reply
            Seacow.ContextParticipantStreamRequest.ContextChangesPendingRequest.Builder changesPendingReplyBuilder = Seacow.ContextParticipantStreamRequest.ContextChangesPendingRequest
              .newBuilder()
              .setContextCoupon(contextChangesPending.getContextCoupon())
              .setDecision("accept");
            // Add it to the queue
            requestQueue.offer(new ActualRequestQueueItem(Seacow.ContextParticipantStreamRequest.newBuilder().setContextChangesPending(changesPendingReplyBuilder).build()));
            break;
          // A reply for `AGENT_CONTEXT_CHANGES_PENDING` expected as well.
          case AGENT_CONTEXT_CHANGES_PENDING:
            // Log then `AGENT_CONTEXT_CHANGES_PENDING` message.
            Seacow.ContextParticipantStreamResponse.AgentContextChangesPendingResponse agentContextChangesPending = value.getAgentContextChangesPending();
            logger.info("Received AGENT_CONTEXT_CHANGES_PENDING: " + agentContextChangesPending.toString());
            // Build a reply with some return values
            Seacow.ContextParticipantStreamRequest.AgentContextChangesPendingRequest.Builder agentChangesReplyBuilder = Seacow.ContextParticipantStreamRequest.AgentContextChangesPendingRequest
              .newBuilder()
              .setAgentCoupon(agentContextChangesPending.getAgentCoupon())
              .setContextCoupon(agentContextChangesPending.getContextCoupon())
              .setDecision("accept")
              .addItemNames("MyReturnValue").addItemValues("foo");
            requestQueue.offer(new ActualRequestQueueItem(Seacow.ContextParticipantStreamRequest.newBuilder().setAgentContextChangesPending(agentChangesReplyBuilder).build()));
            break;
          case APPLICATION_NOTIFICATION:
            // In case of a application notification we're notified that e.g. an application has been started.
            // This event occurs before the application has joined the session and is the earliest notifiication of upcoming
            // participation changes we'll get.

            // First, lets get the actual notification.
            Seacow.ContextParticipantStreamResponse.ApplicationNotification appNotification = value.getApplicationNotification();

            // The we'll log some basic info. This notification can be used to redirect the users attention to the started application,
            // e.g. by bringing it to the foreground.
            logger.info("I was notified about an "+appNotification.getEvent().name()+" for "+appNotification.getApplicationName());
            // Now log some identifying info the app. *Note* that it is not always possible to provide both the window-handle and the process-id.
            logger.info("The application is identified by its window-handle: "+appNotification.getWindowHandle()+", and its process-id: "+appNotification.getProcessId());
            break;
          // We'll handle the rest of the messages by logging to stdout.
          case CONTEXT_CHANGES_ACCEPTED:
            break;
          case CONTEXT_CHANGES_CANCELED:
            break;
          case COMMON_CONTEXT_TERMINATED:
            break;
          case PARTICIPANT_NOTIFICATION:
            break;
          case PING:
            logger.info("Pinged");
            requestQueue.offer(new ActualRequestQueueItem(Seacow.ContextParticipantStreamRequest.newBuilder().setPong(Seacow.ContextParticipantStreamRequest.PongRequest.newBuilder().build()).build()));
            break;
          default:
            logger.info("Received: " + value.getContentCase().toString() + ". With value: " + value.toString());
            break;
        }
      }

      @Override
      public void onError(Throwable t) {
        logger.warning("Error in stream: " + t.getMessage());
        requestQueue.offer(new TerminalRequestQueueItem(t));
      }

      @Override
      public void onCompleted() {
        requestQueue.offer(new TerminalRequestQueueItem(null));
      }
    });

    // After the stream is setup we'll send a regular JoinCommonContext invocation using the same stream.
    Seacow.JoinCommonContextRequest.Builder joinBuilder = Seacow.JoinCommonContextRequest
      .newBuilder()
      // The application name should be an id of a registered application
      .setApplicationName(applicationName)
      // The participant should be an URL and since we're using the streaming gRPC option it's scheme must be `grpc`
      .setParticipant("grpc://" + applicationName)
      // The component identifier is used to select which context manager we're joining
      .setComponentIdentifier(componentId)
      // `survey = true` indicates we're taking an active part in the context
      .setSurvey(shouldSurvey)
      // `wait = true` indicates that we're interested in waiting for any ongoing transactions to complete
      .setWait(shouldWait);

    // Send actual join context request.
    stream.onNext(Seacow.ContextParticipantStreamRequest.newBuilder().setJoinCommonContext(joinBuilder).build());

    // We setup the consumer loop which takes items from the blocking queue and ships them across the stream.
    Thread t = new Thread(new Runnable() {
      @Override
      public void run() {
        while (true) {
          // Consume the queue of requests.
          RequestQueueItem r = null;
          logger.info("About to take from requestQueue");
          try {
            r = requestQueue.take();
          } catch (InterruptedException e) {
            logger.info("Receiving thread interrupted");
            break;
          }
          // If we get a null value then we're done.
          if (r instanceof TerminalRequestQueueItem) break;
          Seacow.ContextParticipantStreamRequest request = ((ActualRequestQueueItem) r).getRequest();
          logger.info("Sending request: " + request.toString());
          // Ship the request on the stream.
          stream.onNext(request);
        }
        logger.info("Stopped processing inbound requests");
      }
    });
    t.start();

    System.out.println("Press enter to quit");
    System.console().readLine();
    stream.onCompleted();
    System.console().readLine();

    logger.info("Session is done for me");
  }

  private String getComponentIdIfNotSpecified(@CommandLine.Option(names = {"--componentId"}, defaultValue = "", description = "The id of the context manager to join") String componentId, SeacowConnection s) {
    RegistryGrpc.RegistryBlockingStub registry = s.getRegistry();

    // If componentId is not specified we'll get the first one
    if (componentId.equals("")) {
      Seacow.OfTypeResponse otr = registry.ofType(Seacow.OfTypeRequest.newBuilder().setTypeName("ContextManager").build());
      if (otr.getIdentifiersCount() > 0) componentId = otr.getIdentifiers(0);
    }
    return componentId;
  }

  // ### Create a new session
  //
  // Demonstrates how to create a new context session providing `tag`, `color` and whether the `fullauto` functionality
  // should be disabled. Each session is backed by a single context manager thus the terms session and context manager
  // are used interchangeably. A note on the weirdness of supplying a `componentId` to this method: It is the responsibility
  // of the ContextManager to create new sessions (and thus new ContextManagers) therefore you must supply a known session id
  // / context manager id to create a new one.
  @CommandLine.Command(mixinStandardHelpOptions = true)
  public void CreateSession(
    @CommandLine.Option(
      names = {"--componentId"},
      defaultValue = "",
      description = "The id of the context manager to create the session on"
    ) String componentId,
    @CommandLine.Option(
      names = {"--tag"},
      required = true,
      description = "The tag of the context manager to create - if a context manager with the given tag already exists it will be returned and no new session/context manager is created"
    ) String tag,
    @CommandLine.Option(
      names = {"--color"},
      description = "The color of the context manager"
    ) String color,
    @CommandLine.Option(
      names = {"--nofullauto"},
      defaultValue = "false",
      description = "If `true` then the FullAutoController will not run in the created session"
    ) boolean noFullAuto
  ) throws SSLException {

    SeacowConnection s = new SeacowConnection(host, port, cert);
    // Get the context session service
    ContextSessionGrpc.ContextSessionBlockingStub cs = s.getContextSession();
    //componentId = getComponentIdIfNotSpecified(componentId, s);


    // Construct the request passing in the parameters from the command line
    Seacow.CreateRequest createRequest = Seacow.CreateRequest.newBuilder()
      //.setComponentIdentifier(componentId)
      //.setTag(tag)
      //.setColor(color)
      //.setNoFullAuto(noFullAuto)
      .build();

    // Now issue the request to create a new session
    Seacow.CreateResponse createResponse = cs.create(createRequest);

    // Note that the session will be pruned (deleted) automatically in a short while unless a context participant
    // joins to keep it alive.
    logger.info(createResponse.toString());
  }

  // ### Read the shared context
  //
  // This example demonstrates how to read the current value of all subjects in the shared context.
  @SuppressWarnings("Duplicates")
  @CommandLine.Command(mixinStandardHelpOptions = true)
  public void ReadState(
    @CommandLine.Option(
      names = {"--componentId"},
      defaultValue = "",
      description = "The id of the context manager to join"
    ) String componentId,
    @CommandLine.Option(
      names = {"--applicationName"},
      required = true,
      description = "The name identifying the application. This should be an identifier from the CMR."
    ) String applicationName
  ) throws SSLException {
    // Get the necessary services.
    SeacowConnection s = new SeacowConnection(host, port, cert);
    ContextManagerGrpc.ContextManagerBlockingStub cm = s.getContextManager();
    ContextDataGrpc.ContextDataBlockingStub cd = s.getContextData();
    componentId = getComponentIdIfNotSpecified(componentId, s);


    // Construct and execute the join request. We'll pretend we have a HTTP server running.
    Seacow.JoinCommonContextResponse joinResponse = cm.joinCommonContext(createScammingJoinRequest(componentId, applicationName));

    // We'll need to use the participant coupon.
    long pc = joinResponse.getParticipantCoupon();

    // Lets see which subjects are in the shared context.
    Seacow.GetItemNamesResponse namesResponse = cd.getItemNames(Seacow.GetItemNamesRequest
      .newBuilder()
      .setComponentIdentifier(componentId)
      // -1 is a *secret* shortcut to MostRecentContextCoupon
      .setContextCoupon(-1)
      .build());

    // Log all found names.
    logger.info("Found " + namesResponse.getNamesCount() + " names in the shared context");
    for (String name : namesResponse.getNamesList()) {
      logger.info("Name:" + name);
    }

    // Lets see which values are then stored for these names.
    Seacow.GetItemValuesResponse values = cd.getItemValues(Seacow.GetItemValuesRequest
      .newBuilder()
      .setComponentIdentifier(componentId)
      .setContextCoupon(-1)
      .addAllItemNames(namesResponse.getNamesList())
      .build());

    // Log all names and values. The resulting list is composed as `[name1, value1, name2, value2, ..., nameN, valueN]`.
    for (String nameOrValue : values.getItemValuesList()) {
      logger.info("NameOrValue:" + nameOrValue);
    }
  }

  // ### Write to the shared context
  //
  // Writing to the shared context is slightly involved as one must go through a complete context transaction to do so.
  // The steps involved here are:
  //
  // 1. Join the common context.
  // 2. Start a context transaction.
  // 3. Specify the changes to make in the transaction.
  // 4. End the transaction and check the results.
  // 5. Publish the changes (commit or rollback).
  // 6. Leave the context.
  @SuppressWarnings("Duplicates")
  @CommandLine.Command(mixinStandardHelpOptions = true)
  public void WriteState(
    @CommandLine.Option(
      names = {"--componentId"},
      defaultValue = "",
      description = "The id of the context manager to join"
    ) String componentId,
    @CommandLine.Option(
      names = {"--applicationName"},
      required = true,
      description = "The name identifying the application. This should be an identifier from the CMR."
    ) String applicationName,
    @CommandLine.Option(
      names = {"--state"},
      description = "The new state"
    ) Map<String, String> state,
    @CommandLine.Option(
      names = {"--delete"},
      description = "State items to delete"
    ) List<String> delete
  ) throws SSLException {
    SeacowConnection s = new SeacowConnection(host, port, cert);
    ContextManagerGrpc.ContextManagerBlockingStub cm = s.getContextManager();
    ContextDataGrpc.ContextDataBlockingStub cd = s.getContextData();
    componentId = getComponentIdIfNotSpecified(componentId, s);


    // 1. Join the common context.
    Seacow.JoinCommonContextResponse joinResponse = cm.joinCommonContext(createScammingJoinRequest(componentId, applicationName));
    long pc = joinResponse.getParticipantCoupon();

    // 2. Start the transaction.
    Seacow.StartContextChangesResponse startChangesResponse = cm.startContextChanges(Seacow.StartContextChangesRequest
      .newBuilder()
      .setComponentIdentifier(componentId)
      .setParticipantCoupon(pc)
      .build());

    // We'll need the coupon for the transaction for later.
    long cc = startChangesResponse.getContextCoupon();

    // 3. Construct and execute a request to update the shared state
    if (state != null) {
      cd.setItemValues(Seacow.SetItemValuesRequest
        .newBuilder()
        .setComponentIdentifier(componentId)
        .setContextCoupon(cc)
        .setParticipantCoupon(pc)
        .addAllItemNames(state.keySet())
        .addAllItemValues(state.values())
        .build()
      );
    }

    //   Delete any requested state items also
    if (delete != null) {
      cd.deleteItems(Seacow.DeleteItemsRequest
        .newBuilder()
        .setComponentIdentifier(componentId)
        .setContextCoupon(cc)
        .setParticipantCoupon(pc)
        .addAllItemNames(delete)
        .build()
      );
    }

    // 4. End the transaction.
    Seacow.EndContextChangesResponse endResponse = cm.endContextChanges(Seacow.EndContextChangesRequest
      .newBuilder()
      .setComponentIdentifier(componentId)
      .setContextCoupon(cc)
      .build());

    logger.info(endResponse.toString());

    // Log the responses we get.
    for (String response : endResponse.getResponsesList()) {
      logger.info("Received response: " + response);
    }

    // 5. Publish changes. For this example we'll just always "accept".
    cm.publishChangesDecision(Seacow.PublishChangesDecisionRequest
      .newBuilder()
      .setComponentIdentifier(componentId)
      .setContextCoupon(cc)
      .setDecision("accept")
      .build());

    // 6. Leave the common context
    cm.leaveCommonContext(Seacow.LeaveCommonContextRequest
      .newBuilder()
      .setParticipantCoupon(pc)
      .setComponentIdentifier(componentId)
      .build());
  }

  // ### Benchmark a regular context transaction
  //
  // The benchmark goes through the following steps:
  //
  // 1. Join the common context.
  // 2. Start a context transaction.
  // 3. Specify the changes to make in the transaction.
  // 4. End the transaction and check the results.
  // 5. Publish the changes (commit or rollback).
  // 6. If benchmark run is completed then exit else go to 2.
  // 7. Leave the context.
  @SuppressWarnings("Duplicates")
  @CommandLine.Command(mixinStandardHelpOptions = true)
  public void BenchmarkTx(
    @CommandLine.Option(
      names = {"--componentId"},
      required = true,
      description = "The id of the context manager to join"
    ) String componentId,
    @CommandLine.Option(
      names = {"--applicationName"},
      required = true,
      description = "The name identifying the application. This should be an identifier from the CMR."
    ) String applicationName,
    @CommandLine.Option(
      names = {"--runs"},
      defaultValue = "10",
      description = "The number of transactions to run."
    ) int runs,
    @CommandLine.Option(
      names = {"--subjects"},
      defaultValue = "foo,bar,bax",
      split = ",",
      description = "The subjects to include in the tx."
    ) List<String> subjects
  ) throws SSLException {
    SeacowConnection s = new SeacowConnection(host, port, cert);
    ContextManagerGrpc.ContextManagerBlockingStub cm = s.getContextManager();
    ContextDataGrpc.ContextDataBlockingStub cd = s.getContextData();

    Stopwatch w = Stopwatch.createStarted();
    Stopwatch txW = Stopwatch.createUnstarted();
    JsonArray txTimings = new JsonArray();
    for (int i = 0; i < runs; i++) {
      JsonObject txTiming = new JsonObject();
      txW.reset().start();

      // 1. Join the common context.
      Seacow.JoinCommonContextResponse joinResponse = cm.joinCommonContext(createScammingJoinRequest(componentId, applicationName));
      long pc = joinResponse.getParticipantCoupon();

      txTiming.addProperty("JoinCommonContext", w.elapsed(TimeUnit.MILLISECONDS));

      w.reset().start();
      // 2. Start the transaction.
      Seacow.StartContextChangesResponse startChangesResponse = cm.startContextChanges(Seacow.StartContextChangesRequest
        .newBuilder()
        .setComponentIdentifier(componentId)
        .setParticipantCoupon(pc)
        .build());

      txTiming.addProperty("StartContextChanges", w.elapsed(TimeUnit.MILLISECONDS));

      // We'll need the coupon for the transaction for later.
      long cc = startChangesResponse.getContextCoupon();

      // 3. Construct and execute a request to update the shared state
      Map<String, String> state = new HashMap<String, String>();
      for (String subject : subjects) {
        state.put(subject, String.valueOf(Math.random() * 100000));
      }

      w.reset().start();
      cd.setItemValues(Seacow.SetItemValuesRequest
        .newBuilder()
        .setComponentIdentifier(componentId)
        .setContextCoupon(cc)
        .setParticipantCoupon(pc)
        .addAllItemNames(state.keySet())
        .addAllItemValues(state.values())
        .build()
      );

      txTiming.addProperty("SetItemValues", w.elapsed(TimeUnit.MILLISECONDS));


      // 4. End the transaction.
      w.reset().start();
      Seacow.EndContextChangesResponse endResponse = cm.endContextChanges(Seacow.EndContextChangesRequest
        .newBuilder()
        .setComponentIdentifier(componentId)
        .setContextCoupon(cc)
        .build());

      txTiming.addProperty("EndContextChanges", w.elapsed(TimeUnit.MILLISECONDS));

      // Log the responses we get.
      for (String response : endResponse.getResponsesList()) {
        logger.info("Received response: " + response);
      }

      // 5. Publish changes. For this example we'll just always "accept".
      w.reset().start();
      cm.publishChangesDecision(Seacow.PublishChangesDecisionRequest
        .newBuilder()
        .setComponentIdentifier(componentId)
        .setContextCoupon(cc)
        .setDecision("accept")
        .build());
      txTiming.addProperty("PublishChangesDecision", w.elapsed(TimeUnit.MILLISECONDS));

      // 6. Leave the common context
      w.reset().start();
      cm.leaveCommonContext(Seacow.LeaveCommonContextRequest
        .newBuilder()
        .setParticipantCoupon(pc)
        .setComponentIdentifier(componentId)
        .build());

      txTiming.addProperty("LeaveCommonContext", w.elapsed(TimeUnit.MILLISECONDS));
      txTiming.addProperty("Total", txW.elapsed(TimeUnit.MILLISECONDS));

      txTimings.add(txTiming);
    }

    logger.info("Timing: " + txTimings);
  }

  // ## Context Action example

  // ### Perform an action
  //
  // You can request an action to be performed by the context manager. It will then route the request to the appropriate
  // action agent and return the result.
  // For this example we'll use the blocking versions of the services and we'll cheat a bit by using the HTTP mapping,
  // but wont setup an actual HTTP server to respond to the context managers requests. This means that we'll get thrown off
  // the manager after a little while.
  @CommandLine.Command(mixinStandardHelpOptions = true)
  public void PerformAction(
    @CommandLine.Option(
      names = {"--componentId"},
      defaultValue = "",
      description = "The id of the context manager to join"
    ) String componentId,
    @CommandLine.Option(
      names = {"--applicationName"},
      required = true,
      description = "The name identifying the application/cp you are. This should be an identifier from the CMR."
    ) String applicationName,
    @CommandLine.Option(
      names = {"--action"},
      description = "The ids of the actions to perform",
      required = true
    ) String[] actions,
    @CommandLine.Option(
      names = {"--inputs"},
      description = "The inputs to provide to the action"
    ) Map<String, String> inputs,
    @CommandLine.Option(
            names = {"--parallel"},
            description = "The inputs to provide to the action"
    ) boolean parallel
  ) throws SSLException {

    SeacowConnection s = new SeacowConnection(host, port, cert);
    // Get a context manager service to be able to join the common context.
    ContextManagerGrpc.ContextManagerBlockingStub cm = s.getContextManager();
    // Get a context action service to be able to perform actions.
    ContextActionGrpc.ContextActionBlockingStub ca = s.getContextAction();
    // Select the first CM/Session if none is given
    componentId = getComponentIdIfNotSpecified(componentId, s);


    // Pretend we have a HTTP server running.
    Seacow.JoinCommonContextRequest joinRequest = createScammingJoinRequest(componentId, applicationName);

    // Join the common context. Only context participants are allowed to request actions to be performed.
    Seacow.JoinCommonContextResponse joinResponse = cm.joinCommonContext(joinRequest);
    logger.info("Joined with: " + joinResponse.toString());

    // We'll need the participant coupon when asking the context manager to perform an action.
    long pc = joinResponse.getParticipantCoupon();


    // If parallel is set, we'll perform the actions in parallel.
    if (parallel) {
      List<Callable<Void>> tasks = new ArrayList<>();
      for (String action : actions) {
        String finalComponentId = componentId;
        tasks.add(() -> {
          PerformAction(finalComponentId, inputs, ca, pc, action);
          return null;
        });
      }
      try {
        ExecutorService executor = Executors.newFixedThreadPool(actions.length);
        executor.invokeAll(tasks);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    // else we run them sequentially.
    else {
      for (String action : actions) {
        logger.info("Performing: " + action);
        PerformAction(componentId, inputs, ca, pc, action);
      }
    }

    // We really need to execute a LeaveCommonContext for quick turn-around times.
    cm.leaveCommonContext(Seacow.LeaveCommonContextRequest.newBuilder()
    .setComponentIdentifier(componentId)
    .setParticipantCoupon(pc)
    .build());
  }

  private static void PerformAction(String componentId, Map<String, String> inputs, ContextActionGrpc.ContextActionBlockingStub ca, long pc, String action) {
    // Construct the request to have an action performed.
    Seacow.PerformRequest.Builder performRequestBuilder = Seacow.PerformRequest
            .newBuilder()
            .setComponentIdentifier(componentId)
            .setParticipantCoupon(pc)
            .setActionIdentifier(action);

    // If the action needs an input to run we can also provide these in the request.
    if (inputs != null && inputs.size() > 0) {
      // Iterate the provided inputs and add to request.
      for (String name : inputs.keySet()) {
        performRequestBuilder.addInputNames(name);
        performRequestBuilder.addInputValues(inputs.get(name));
      }
    }

    // Execute the request.
    Seacow.PerformResponse performResponse = ca.perform(performRequestBuilder.build());
    logger.info("Received a perform response: " + performResponse.toString());
  }

  // ## Utility functions

  // Create "scamming" join request. The scam is that we're not really running a context participant at `localhost:1234`.
  private Seacow.JoinCommonContextRequest createScammingJoinRequest(@CommandLine.Option(names = {"--componentId"}, required = true, description = "The id of the context manager to join") String componentId, @CommandLine.Option(names = {"--applicationName"}, required = true, description = "The name identifying the application. This should be an identifier from the CMR.") String applicationName) {
    return Seacow.JoinCommonContextRequest
      .newBuilder()
      .setComponentIdentifier(componentId)
      .setApplicationName(applicationName)
      .setWait(true)
      .setSurvey(true)
      // This endpoint is a scam.
      .setParticipant("http://localhost:1234")
      .build();
  }

  @Override
  public void run() {
    // Show help message if no sub-command is given
    CommandLine.usage(new RootCmd(), System.out);
  }
}
