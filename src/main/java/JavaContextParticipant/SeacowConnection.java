// # Seacow
// The Seacow class is a simple wrapper for the gRPC channel and clients for the gRPC services defined in the seacow.proto file.

package JavaContextParticipant;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import seacow.*;

import javax.net.ssl.SSLException;
import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class SeacowConnection {

    private static final Logger logger = Logger.getLogger(SeacowConnection.class.getName());

    private final ContextManagerGrpc.ContextManagerStub cmAsync;
    private final ContextManagerGrpc.ContextManagerBlockingStub cm;
    private final ContextDataGrpc.ContextDataBlockingStub cd;
    private final ContextActionGrpc.ContextActionBlockingStub ca;
    private final ContextSessionGrpc.ContextSessionBlockingStub cs;
    private final RegistryGrpc.RegistryBlockingStub registry;

    private final ManagedChannel channel;


    public SeacowConnection() throws SSLException {
        // When connecting without giving a `host` or `port` we'll use `localhost:9000`
        this("localhost", 9000, null);
    }

    public SeacowConnection (String host, int port, String cert) throws SSLException {
        // When connecting we need a `host` and a `port` to connect to
        ManagedChannelBuilder<?> builder = cert != null && !cert.equalsIgnoreCase("")? NettyChannelBuilder
          .forAddress(host, port)
          .sslContext(GrpcSslContexts.forClient().trustManager(new File(cert)).build())
          : ManagedChannelBuilder.forAddress(host, port).usePlaintext();
        channel = builder.build();

        // Create blocking stubs for services required for example uses.
        // The context manager service (non-streaming) - it implements the Context Manager interface.
        cm = ContextManagerGrpc.newBlockingStub(channel);

        // Create a blocking stub for the context data service.
        cd = ContextDataGrpc.newBlockingStub(channel);

        // Create a blocking stub for the context action service.
        ca = ContextActionGrpc.newBlockingStub(channel);

        // Create a blocking stub for the context session service.
        cs = ContextSessionGrpc.newBlockingStub(channel);

        // The registry contains convenience methods for accessing the Context Management Registry (CMR) through Manatetee.
        // The CMR can also be accessed directly, but this is rarely needed for Manatee participants.
        registry = RegistryGrpc.newBlockingStub(channel);

        // Create a non-blocking (async) stub for the context manager service. This is used for gRPC bi-directional streaming.
        cmAsync = ContextManagerGrpc.newStub(channel);
    }

    public ContextManagerGrpc.ContextManagerBlockingStub getContextManager() {
        return cm;
    }

    public ContextSessionGrpc.ContextSessionBlockingStub getContextSession() {
        return cs;
    }

    public RegistryGrpc.RegistryBlockingStub getRegistry() {
        return registry;
    }

    public ContextManagerGrpc.ContextManagerStub getContextManagerAsync() { return cmAsync; }

    public ContextDataGrpc.ContextDataBlockingStub getContextData() { return cd; }

    public ContextActionGrpc.ContextActionBlockingStub getContextAction() { return ca; }

    public void Disconnect() throws InterruptedException {
        if (channel != null) {
            logger.info("shutting down");
            // Disconnect cleanly with a 5 second timeout
            channel.shutdown().awaitTermination(30, TimeUnit.SECONDS);
            logger.info("shutdown completed");
        }
    }


}
